/*
    Copyright 2006-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Declaration of terminator classes.

// $Id$

#ifndef __terminator__
#define __terminator__

namespace mcrx {
  class terminator;
  class dummy_terminator;
}

/** Abstract base class for terminators. */
class mcrx::terminator {
public:
  enum t_type {false_ = 0, true_ = 1, restart_ = 2};

  virtual ~terminator () {};
  virtual t_type operator() () const = 0;
};

/** A terminator that never terminates anything. */
class mcrx::dummy_terminator: public terminator {
public:
  virtual t_type operator() () const {return false_;};
};

#endif
