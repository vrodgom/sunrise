 /*
    Copyright 2006-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/** \defgroup sfrhist Star formation history integration code (sfrhist)
    The sfrhist executable calculates SEDs for the particles in a
    GADGET snapshot.

    The configuration options and output file format for sfrhist are
    described on the \ref sfrhist-readme page.
*/

// $Id$

/** \file
    Declaration of the sfrhist class.  \ingroup sfrhist */

// $Id$

#ifndef __sfrhist__
#define __sfrhist__

#include <vector>
#include "preferences.h"
#include "sfrhist_snapshot.h"
#include "functors.h"

namespace mcrx {
  class sfrhist;
};

namespace CCfits {
  class HDU;
  class ExtHDU;
};

namespace boost{
  template<typename> class shared_ptr;
};

/** The main sfrhist class which does the SED calculation. 
    \ingroup sfrhist */
class mcrx::sfrhist {
protected:
  static const std::string version;
  
  /** \name Preferences objects
      Keeps keywords of different categories.  */
  /// @{
  Preferences& p; ///< Keywords in configuration file.
  Preferences& gadget; ///< Keywords from GADGET configuration file.
  std::vector<Preferences> merger; ///< merger setup information
  /// @}
  
  sfrhist_snapshot& snap; ///< The simulation snapshot.

  bool is_merger; ///< True if the snapshot contains a merger.
  int nobject; ///< Number of objects in the snapshot.

  bool read_merger_file (const std::string &);

  void calculate_centers (sfrhist_snapshot&,
			  const std::vector<boost::shared_ptr<Snapshot> >&) ;
  void write_history (CCfits::HDU &);

  boost::shared_ptr<mcrx::functors::T_particle_functor>
  age_functor (int object, Snapshot::species s) const;
  boost::shared_ptr<mcrx::functors::T_particle_functor>
  z_functor (int object, Snapshot::species s) const;
  std::vector<boost::shared_ptr<mcrx::sfrhist_snapshot::T_age_metallicity_functor> >
  compound_functors(Snapshot::species s) const;

public:
  sfrhist (Preferences&, Preferences&, sfrhist_snapshot&);
  void assign_age_metallicity();
  void write_output_files (const std::string&);

};


#endif


